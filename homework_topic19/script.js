const first = prompt('Ведите первое число');
let a = Number(first);
if (first) {
    if (typeof a === 'number') {
        if (isNaN(a)) {
            console.log('Некорректный формат!');
        } else {
            const sign = prompt( 'Введите знак');
            const second = prompt('Ведите второе число');
            let b = Number(second);
            if (second) {
                if (typeof b === 'number') {
                    if (isNaN(b)) {
                        console.log('Некорректный формат!');
                    } else {
                        let res;
                        switch (sign) {
                            case '+':res = a + b; break;
                            case '-':res = a - b; break;
                            case '/':res = a / b; break;
                            case '*':res = a * b; break;
                            default: console.log('Программа не поддерживает данный знак'); break;
                        }
                        console.log('Результат', res);
                    }
                }
            } else {
                console.log('Нет второго числа');
            }
        }
    }
} else {
    console.log('Нет первого числа');
}