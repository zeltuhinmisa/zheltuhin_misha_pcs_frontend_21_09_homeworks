'use strict';
let globalList;
let counter = 1;
let arrFunctions = [
    getUsersData,
    getLastnameUsers,
    getDataUsersLastnameAsLetterF,
    getNameAndLastnameUsersWithReduce,
    getUserKeys
]


getFetchData();

function getFetchData() {
    fetch('https://reqres.in/api/users?per_page=12')
        .then((response) => response.json())
        .then((body) => {
            let data = body.data;
            run(data);
        })
}

function run(list) {
    globalList = list;
    arrFunctions.forEach((func) => {
        consolePointer();
        func();
        counter++;
    })
}

function getUsersData() {
    console.log(globalList);

}

function getLastnameUsers() {
    globalList.forEach((item) => {
        console.log(item.last_name);
    });
}

function getDataUsersLastnameAsLetterF() {
    globalList.forEach((item) => {
        if (item.last_name[0] === 'F') {
            console.log(item);
        }
    })

}

function getNameAndLastnameUsersWithReduce() {
    let isLast = globalList[globalList.length - 1],
        initialValue = '';
    let firstAndLastNames = globalList.reduce((names, user) => {
        if (user !== isLast)
            names += user.first_name + ' ' + user.last_name + ', ';
        else
            names += user.first_name + ' ' + user.last_name + '.';
        return names;

    }, initialValue);
    console.log('Наша база содержит данные пользователей: ' + firstAndLastNames);
}

function getUserKeys() {
    let usersKeys = Object.keys(globalList[0]);
    usersKeys.forEach((item) => {
        console.log(item);
    });
}

function consolePointer() {
    console.log('-----------');
    console.log('Пункт № ' + counter +':');
    console.log('-----------');
}
