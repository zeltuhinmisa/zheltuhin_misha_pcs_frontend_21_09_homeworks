import * as React from 'react';
import { Routes, Route } from 'react-router-dom';
import Goods  from './component/Goods/Goods';
import Header  from './component/Header/Header';
import Auth from './component/Auth/Auth';
import NotFound from "./component/NotFound/NotFound";
import './App.css';
import Card from "./component/Card/Card";
import AdminPanel from "./component/AdminPanel/AdminPanel";

const cardAArray = [
    {
        image: 'card1.jpg',
        price: 3000,
        characteristic: 'Состав: верх-нубук'
    },
    {
        image: 'card2.jpg',
        price: 5000,
        characteristic: 'Состав: подкладка-трикотаж'
    },
    {
        image: 'card3.jpg',
        price: 7000,
        characteristic: 'Состав: верх-нубук'
    }
];


function App() {
    return(
        <>
            <Header/>
                <Routes history={' '}>
                    <Route path='/'  element={<Goods goods={cardAArray}/>}/>
                    <Route path='/auth' element={<Auth/>}/>
                    <Route path='*' element={<NotFound/>}/>
                    <Route path='/card/:id' element={<Card/>}/>
                    <Route path='/admin/panel' element={<AdminPanel/>}/>
                </Routes>
        </>
    );
}

export default App;

