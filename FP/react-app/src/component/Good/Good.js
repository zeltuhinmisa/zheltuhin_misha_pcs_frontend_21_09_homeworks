import React from "react";
// import './Css/Goods.module.css';



function Good({ image, characteristic, price }) {
        return (

            <div className="assortment__product">
                <div className="assortment__product_image">
                    <img className="assortment__product_image_scale" src={`./Img/${image}`} alt={' '}/>
                </div>
                <p className="assortment__product_characteristic">
                    {characteristic}
                </p>
                <div className="assortment__product_buy-section">
                    <p className="assortment__product_price">{price} руб.</p>
                    <button className="assortment__product_buy">В корзину</button>
                </div>

            </div>
        );
}

export default Good;
