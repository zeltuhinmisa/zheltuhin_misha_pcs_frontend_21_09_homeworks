import React from "react";
// import './Css/Goods.module.css';
import Good from "../Good/Good";


function Goods() {
    return (
        <div className="main-container">
            <div className="category-container">
                <div className="categories-list">
                    <div className="categories">
                        <ul>
                            <li>Кроссовки</li>
                            <li>Кеды</li>
                            <li>Сапоги</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div className="assortment-container">
                <div className="goods">
                        <div className="good">
                            <div className="good-logo">

                            </div>
                            <div className="good-name">
                                Название товара
                            </div>
                            <div className="good-description">
                                Описание
                            </div>
                            <div className="good-action">
                                <div className="good-price">
                                    2000 Р
                                </div>
                                <div className="to-cart">
                                    <img src="./cart.png" alt=""/>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    );
}

export default Goods;
