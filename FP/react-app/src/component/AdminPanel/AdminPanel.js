function AdminPanel() {
    return (
        <div className="good-cart-container">
            <p className="admin-label">Администативная панель</p>
            <div className="goods-table-container">
                <div id="create-good">✚</div>
                <table className="goods-table">
                    <thead className="goods-table-head">
                    <tr>
                        <td>ID</td>
                        <td>Категория</td>
                        <td>Название товара</td>
                        <td>Описание</td>
                        <td>Цена</td>
                        <td>Ссылка на картинку</td>
                        <td></td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>Кроссовки</td>
                        <td>Nike</td>
                        <td>Как тапки только лучше</td>
                        <td>2000</td>
                        <td>ссылка</td>
                        <td>
                            <div id="change-good">✎</div>
                        </td>
                        <td>
                            <div id="delete-good">✖</div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default AdminPanel;