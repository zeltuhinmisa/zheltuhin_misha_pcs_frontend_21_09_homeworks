import React from 'react';
import logo from './Img/Logo.png'  ;
import {Link} from "react-router-dom";


function Header() {
    return (
        <div className="header-container">
            <img className="nav-container_logo" src={logo}/>
            <div className="nav-container">
                <ul>
                    <li><Link to="/admin/panel">Админ-панель</Link></li>
                    <li>Каталог</li>
                    <li> <Link to="/auth"> Регистрация </Link> </li>
                </ul>
            </div>
        </div>
            )
}

export default Header;


