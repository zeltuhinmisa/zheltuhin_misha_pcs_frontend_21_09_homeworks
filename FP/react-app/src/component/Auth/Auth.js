import React from "react";
import styles from './Css/Auth.module.css';
import script from './Script/js';

function Auth() {
    return (
        <div>
            <div className={styles["enter-form"]}>
                <h1 className={styles["enter-form__h1"]}>Регистрация</h1>
                <div className={styles["enter-form__login"]}>
                    <label className={styles["enter-form__login-label"]}>Email</label>
                    <p className={styles["enter-form__email-star"]} id="enter-form__email-star">*</p>
                    <input className={styles["enter-form__login-input"]} type="email" id="enter-form__email"
                           placeholder="Введите email"/>
                    <p className={styles["enter-form__error-message_login"]} id="enter-form__error-message_login"/>
                </div>
                <div className={styles["enter-form__password"]}>
                    <p className={styles["enter-form__password-star"]} id="enter-form__password-star">*</p>
                    <input className={styles["enter-form__password-input"]} type="password"
                           id="enter-form__password-input" placeholder="Введите пароль"/>
                    <p className={styles["enter-form__error-message_password"]}
                       id="enter-form__error-message_password"/>
                    <label className={styles["enter-form__password-label"]}>Password</label>
                </div>
                <div className={styles["enter-form__checkbox"]}>
                    <input className={styles["enter-form__checkbox-input"]} type="checkbox"
                           id="enter-form__checkbox-input"/>
                    <div className={styles["enter-form__checkbox-square"]} id="enter-form__checkbox-square"/>
                    <p className={styles["enter-form__checkbox_star"]} id="enter-form__checkbox_star">*</p>
                    <label className={styles["enter-form__checkbox-label"]}>Я согласен с правилами пользования
                        приложением</label>
                    <p className={styles["enter-form__checkbox_error-checkbox"]}
                       id="enter-form__checkbox_error-checkbox"/>
                </div>
                <div className={styles["enter-form__button"]}>
                    <button className={styles['enter-form__button_background_blue']}/>
                    <p className={styles["enter-form__button_paragraph"]}>Регистрация</p>
                </div>
            </div>
            <div className={'enter-form__background-colors'}></div>
        </div>
    );
}

export default Auth;