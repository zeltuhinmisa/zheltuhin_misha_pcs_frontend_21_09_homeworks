import React from "react";

function Card() {
    return (
        <div className="Card-page">
            <div className="Card-page__main">
                <div className="main__image">
                    <img className="main__image_sneaker" src="./card1.jpg"/>
                </div>
                <table className="good-description-table">
                    <thead className="good-description-table-head">
                    <tr>
                        <td>Общие характеристики</td>
                        <td></td>
                    </tr>
                    </thead>

                    <tbody className="good-description-table-body">
                    <tr>
                        <td>Название</td>
                        <td>Nike</td>
                    </tr>
                    <tr>
                        <td>Категория</td>
                        <td>кроссовки</td>
                    </tr>
                    <tr>
                        <td>Описание</td>
                        <td>Описание кроссовок...</td>
                    </tr>
                    <tr>
                        <td>Цена</td>
                        <td>3000</td>
                    </tr>
                    </tbody>

                </table>
            </div>
        </div>
    );
}

export default Card;
