'use strict'
function fibonacci() {
    let fibStrg = localStorage.getItem('fib');
    if (fibStrg) {
        let a = 1;
        let b = 1;
        let c;
        for (let i = 0; i < fibStrg; i++) {
            c = a + b;
            a = b;
            b = c;
        }
        let newFib = Number(fibStrg);
        newFib++;
        localStorage.removeItem('fib');
        localStorage.setItem('fib', String(newFib++));
        return c;
    } else {
        localStorage.setItem('fib', 1);
        return 1;
    }
}

function cleanStorage() {
    localStorage.removeItem('fib');
}
cleanStorage();
